@Library('pipeline-my-library')_

def PROJECT = ""
def GITLAB_ACTION = ""
def BRANCH = ""

// tools {
//         maven 'apache-maven-3.5.3'
// }
environment {
    JAVA_HOME = '/opt/jdk/1.8.0_171/jdk1.8.0_171'
}

options {
    buildDiscarder(logRotator(numToKeepStr:'5'))
}


pipeline {
    agent any

    parameters {
        choice(choices: 'pim\necom', description: 'Which Project ?', name: 'PROJECT_')
        choice(choices: '10.42.1.131\n10.42.1.132\n10.42.1.133\n10.42.1.134', description: 'Which App Server ?', name: 'APP_SERVER')
        choice(choices: 'local.properties.int1\nlocal.properties.int2', description: 'Which local properties to use ?', name: 'LOCAL_PROPERTIES')
        choice(choices: 'origin/master', description: 'Which Branch ?', name: 'TAG')
        choice(choices: 'initialize\nupdatesystem', description: 'Wich Task ?', name: 'TASK')
        string(defaultValue: "localextensions.xml", description: 'Which local extensions to use ?', name: 'LOCAL_EXTENSIONS')
        string(defaultValue: "private.properties", description: 'Which private properties ?', name: 'PRIVATE_PROPERTIES')
        string(defaultValue: false, description: 'Upload artifacts to Nexus ?', name: 'PACKAGE')
        string(defaultValue: false, description: 'Make a release ?', name: 'RELEASE')
        string(defaultValue: false, description: 'Deploy to App Server', name: 'DEPLOY')
    }

    triggers {
        gitlab(
          triggerOnPush: true, 
          triggerOnMergeRequest: true,
          branchFilterType: 'All',
          secretToken: generateToken()
        )
    }

    stages {

        stage("Build") {
          when {
              // expression { BRANCH == 'master' }
              expression { GITLAB_ACTION == 'PUSH' }
          }
          steps {
              script {
                PROJECT = getProject(env.gitlabActionType, env.gitlabBefore, env.gitlabAfter, env.gitlabMergeRequestLastCommit, env.GIT_COMMIT, env.WORKSPACE)
                GITLAB_ACTION = env.gitlabActionType
                BRANCH = env.gitlabBranch
              }
              sh "mvn -pl ${PROJECT} compile"
          }
        }

        stage("Check Compile"){
          when {
              expression { BRANCH == 'master' }
              expression { GITLAB_ACTION == 'PUSH' }
          }
          steps {
              echo "${PROJECT}"
              script {
                prepareFiles("check-compile", env.WORKSPACE, "${PROJECT}")
              }
              sh "mvn -pl ${PROJECT} -U -DskipPlatform -DskipCompile -Dtest=true generate-test-resources"
          }
        }

        stage("Unit Tests") {
            steps {
              echo "${PROJECT}"
              script {
                prepareFiles("sonar", env.WORKSPACE, "${PROJECT}")
              }
              sh """
                if [ "${PROJECT}" != "pim,ecom" ]; then
                  cd "$WORKSPACE/${PROJECT}"
                  PROJECT_VERSION=$(echo "cat //*[local-name()='project']/*[local-name()='version']" | xmllint --shell pom.xml | sed '/^\/ >/d' | sed 's/<[^>]*.//g' | grep  "^[0-9].*")
                  echo "PROJECT_VERSION = $PROJECT_VERSION"
                  ant unittests -Dtestclasses.packages=com.mcm.*
                fi 
              """
            }
        }

        stage("Integration Tests"){
          steps {
            echo "Integration Tests"
          }
        }

        stage("Sonar Analysis"){
            steps {
              echo "${PROJECT}"
              script {
                prepareFiles("sonar", env.WORKSPACE, "${PROJECT}")
              }
              sh """
                if [ "${PROJECT}" != "pim,ecom" ]; then
                  cd "$WORKSPACE"
                  PROJECT_VERSION=$(echo "cat //*[local-name()='project']/*[local-name()='version']" | xmllint --shell pom.xml | sed '/^\/ >/d' | sed 's/<[^>]*.//g' | grep  "^[0-9].*")
                  echo "PROJECT_VERSION = $PROJECT_VERSION"
                  cd "$WORKSPACE/${PROJECT}/hybris/bin/custom"
                  mvn --batch-mode -DskipCompile -X verify sonar:sonar -f ${PROJECT}_coverage.xml -s settings_${PROJECT}.xml -Dsonar.projectVersion=$PROJECT_VERSION -Dsonar.jacoco.reportPath=/tmp/coverage/jacoco${PROJECT}.exec
                fi 
              """
            }
        }

        stage("Sonar Analysis On Merge"){
          when {
              expression { BRANCH == 'master' }
              expression { GITLAB_ACTION == 'MERGE' }
          }
          script {
                prepareFiles("merge-sonar", env.WORKSPACE, "${PROJECT}")
              }
              sh """
                if [ "${PROJECT}" != "pim,ecom" ]; then
                  curl --header "Private-Token: 3zHyFWeeGSu3Wzvtcy5M" --header "Sudo sonarqube" https://gitlab.rabat.sqli.com/api/v3/projects/$gitlabMergeRequestTargetProjectId/merge_requests/$gitlabMergeRequestId/commits > merge_result.json
                  commits=$(cat merge_result.json | jq -r ".[].id" )
                  for commit in $commits 
                  do 
                    cd "$WORKSPACE/${PROJECT}/hybris/bin/custom"
                    mvn --batch-mode -X verify sonar:sonar -f pom_${PROJECT}.xml -s settings_${PROJECT}.xml -Dsonar.sourceEncoding="UTF-8" -Dsonar.analysis.mode=preview -Dsonar.gitlab.ignore_certificate=true -Dsonar.gitlab.url=https://10.42.0.99 -Dsonar.gitlab.user_token=2GyiLJYwGECnNJzU33d8 -Dsonar.gitlab.commit_sha=$commit -Dsonar.gitlab.ref_name=$gitlabSourceBranch -Dsonar.gitlab.project_id=$gitlabMergeRequestTargetProjectId -Dsonar.gitlab.disable_global_comment=true
                  done
                fi 
              """
          }
        }

        stage("Package"){
          when {
                expression { params.PACKAGE == true }
            }
          steps {
             sh "mvn -pl ${PROJECT_} deploy"
          }
        }

        stage("Deploy"){
          when {
                expression { BRANCH == 'master' }
                expression { params.DEPLOY == true }
            }
          sh "mvn -pl ${PROJECT_} -s /opt/apache-maven-3.5.3/conf/settings.xml -DLOCAL_PROPERTIES=${LOCAL_PROPERTIES} -DTASK=${TASK} -DLOCAL_EXTENSIONS=${LOCAL_EXTENSIONS} -DAPP_SERVER=${DAPP_SERVER} -DTAG=origin/master -DPRIVATE_PROPERTIES=${PRIVATE_PROPERTIES} '-DANT_OPTS="-Xmx2500m -Xms256m -XX:MaxPermSize=1500m"' -U -B clean site-deploy"
        }

        stage("Release"){
            when {
                expression { BRANCH == 'master' }
                expression { params.RELEASE == true }
            }
            sh "mvn -X initialize release:clean release:prepare -DpreparationGoals=validate -DignoreSnapshots=true release:perform"

        }
    }
}


